#!/bin/bash
########################################################
# Script de provisionamento para vms v1.0              #
# Autor: Guilherme Silva Martins - Maio 2020           #
# Cria um usuário, habilita o sudo para todos os       #
# usuários e instala o ansible para executar playbooks #
########################################################

logfile='/var/log/provision.log'
DISTRO=$( cat /etc/*-release | tr [:upper:] [:lower:] | grep -Poi '(debian|ubuntu|red hat|centos)' | uniq )

function LOG(){
	echo "[`date \"+%d-%m-%Y %H:%M:%S:%s\"`] [Provision] - $1" >> $logfile
}

function createSudoUsers(){
	

	case "$DISTRO" in
        	debian|ubuntu) apt-get -y install sudo ;;
        	"red hat"|centos) yum -y install sudo ;;
        	*) LOG "Distro não suportada" ;;
	esac

	#Libera o sudo para todos os usuários criados
	LOG "Habilitando usuários para sudo"
	sed -Ei 's/%sudo\s*ALL=\(ALL\:ALL\) ALL$/%sudo   ALL=(ALL:ALL) NOPASSWD:ALL/g' /etc/sudoers

	#Cria um usuário chamado automation
	LOG "Criando usuário automation caso não exista"
	getent passwd automation > /dev/null
	if [ "$?" -ne 0 ]; then
  		useradd -d /home/automation -m -s /bin/bash automation
  		gpasswd -a automation sudo
		LOG "Usuário criado com sucesso e adicionado ao grupo sudo"
	else
		LOG "Usuário automation ja existe"
	fi
}

function InstallAnsibleRedHat(){
	
	LOG "Instalando pacotes necessários"
	yum -y update && yum -y install git gcc python3.x86_64 vim curl net-tools wget
	wget https://bootstrap.pypa.io/get-pip.py && python get-pip.py && pip install packaging

	LOG "Clonando repositório..."
	git clone https://github.com/ansible/ansible.git
	cd ./ansible && make && rpm -Uvh ./rpm-build/ansible-*.noarch.rpm

        if [ $? -eq 0 ]; then
                LOG "Ansible Instalado com sucesso"
        else 
                LOG "Falha na instalação, verifique!"
        fi

}

function InstallAnsibleDebian(){
	
	LOG "Adicionando repositório"
	sudo echo 'deb http://ppa.launchpad.net/ansible/ansible/ubuntu trusty main >> /etc/apt/sources.list'
	sudo apt-get update && apt-get install -y gnupg2

	LOG "Validando chave do repositório"
	sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 93C4A3FD7BB9C367
	
	LOG "Instalando ansible..."
	apt-get -y install ansible

	if [ $? -eq 0 ]; then
		LOG "Ansible Instalado com sucesso"
	else 
		LOG "Falha na instalação, verifique!"
	fi
}

createSudoUsers

case "$DISTRO" in
	debian|ubuntu) LOG "Instalando ansible (Debian|Ubuntu)" && InstallAnsibleDebian	;;
        "red hat"|centos) LOG "Instalando ansible (RedHat|CentOS)" && InstallAnsibleRedHat	;;
	*) LOG "Distro não suportada" ;;
esac
